import skmultiflow
from skmultiflow.lazy import KNNClassifier
from skmultiflow.utils import get_dimensions, SlidingWindow
import numpy as np


class MyKNNClassifier(KNNClassifier):
    """ Extended k-Nearest Neighbors classifier with weighted voting in
     prediction and standardization in fitting.

    """

    def __init__(self, n_neighbors=5, max_window_size=1000, leaf_size=30,
                 metric='euclidean',
                 standardize=False,
                 weighted_vote=False,
                 standardization_window='landmark',
                 standardization_debug=False,
                 ):
        super().__init__(n_neighbors, max_window_size, leaf_size, metric)
        self.standardize = standardize
        self.weighted_vote = weighted_vote

        if standardization_window == 'landmark':
            self._n_seen = 0  # number of samples seen
            self._sum = 0.  # sum of samples
            self._ssq = 0.  # sum of squares of samples

        elif standardization_window == 'sliding':
            raise NotImplementedError(
                'Sliding window statistics have not been implemented yet.')
        else:
            raise NotImplementedError(
                f'Standardization mode "{self.standardization_window}" has'
                f' not been implemented yet.')

        self.standardization_window = standardization_window

        self._ideal_window = None
        self.standardization_debug = standardization_debug

    def partial_fit(self, X, y, classes=None, sample_weight=None):
        r, c = get_dimensions(X)

        if classes is not None:
            self.classes = list(set().union(self.classes, classes))

        if self.standardize:
            X = self._standardize(X, update=True)

        for i in range(r):
            self.data_window.add_sample(X[i], y[i])

        return self

    def predict_proba(self, X):
        r, c = get_dimensions(X)
        if self.data_window is None or self.data_window.size < self.n_neighbors:
            # The model is empty, defaulting to zero
            return np.zeros(shape=(r, 1))
        proba = []

        self.classes = list(set().union(self.classes,
                                        np.unique(self.data_window.targets_buffer.astype(np.int))))

        if self.standardize:
            X = self._standardize(X, update=False)

        new_dist, new_ind = self._get_neighbors(X)
        for i in range(r):
            votes = [0.0 for _ in range(int(max(self.classes) + 1))]
            for index in new_ind[i]:
                if self.weighted_vote:
                    feature = self.data_window.features_buffer[index]
                    weight = 1. / np.linalg.norm(X[i] - feature)
                else:
                    weight = 1.
                votes[int(self.data_window.targets_buffer[index])] += \
                    weight / len(new_ind[i])
            proba.append(votes)

        return np.asarray(proba)

    def _standardize(self, x, copy=True, update=True):
        if self.standardization_window == 'landmark':
            x_standardized, mean, std = self._landmark_standardize(
                x, copy=copy, update=update)
        else:
            raise NotImplementedError(
                f'Standardization mode "{self.standardization_window}" has'
                f' not been implemented yet.')

        if self.standardization_debug:
            x_ideal, mean_ideal, std_ideal = self._ideal_standardize(
                x, copy=copy, update=update)
            diff_mean = np.abs(mean_ideal - mean)
            diff_std = np.abs(std_ideal - std)
            print(f'{update=}, {diff_mean=}, {diff_std=}')

        return x_standardized

    def _landmark_standardize(self, x, copy=True, update=True):
        if copy:
            x = x.copy()

        n_seen = self._n_seen + len(x)
        sum_seen = self._sum + x.sum(axis=0)
        ssq_seen = self._ssq + (x * x).sum(axis=0)

        if update:
            self._n_seen = n_seen
            self._sum = sum_seen
            self._ssq = ssq_seen

        mean = sum_seen / n_seen
        var = (ssq_seen - n_seen * mean * mean) / (n_seen - 1)
        std = np.sqrt(var)

        x_standardized = (x - mean) / std
        np.nan_to_num(x_standardized, copy=False, posinf=3., neginf=-3.)

        return x_standardized, mean, std

    def _ideal_standardize(self, x, copy=True, update=True):
        if copy:
            x = x.copy()

        if update:
            if self._ideal_window is None:
                self._ideal_window = np.vstack((x,))
            else:
                self._ideal_window = np.vstack((self._ideal_window, x))

            features = self._ideal_window
        else:
            if self._ideal_window is None:
                features = np.vstack((x,))
            else:
                features = np.vstack((self._ideal_window, x))

        mean = features.mean(axis=0)
        std = features.std(axis=0) + 1e-7

        return (x - mean) / std, mean, std

    def set_landmark(self):
        self._n_seen = 0
        self._sum = 0.
        self._ssq = 0.

        if self.standardization_debug:
            self._ideal_window = None
