import os
import sys

from skmultiflow.data import WaveformGenerator
from skmultiflow.trees import HoeffdingTreeClassifier
from skmultiflow.evaluation import EvaluatePrequential
from skmultiflow.lazy import KNNClassifier, KNNADWINClassifier
from skmultiflow.bayes import NaiveBayes
from skmultiflow.meta import LeveragingBaggingClassifier
from skmultiflow.meta import OzaBaggingADWINClassifier
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.colors import Colormap
from skmultiflow.data import FileStream
import numpy as np

import my_knn_classifier as my_knn


def main():
    print(f'Matplotlib backend is {matplotlib.get_backend()}')
    plt.rcParams['figure.dpi'] = 192

    # 1. Create a stream
    stream = WaveformGenerator()
    stream.prepare_for_use()

    # 2. Instantiate the HoeffdingTree classifier
    ht = HoeffdingTreeClassifier()
    mk = my_knn.MyKNNClassifier()

    # 3. Setup the evaluator
    evaluator = EvaluatePrequential(show_plot=True,
                                    pretrain_size=200,
                                    max_samples=20000)

    # 4. Run evaluation
    evaluator.evaluate(stream=stream, model=ht)


def show_recall(evaluator, model_names):
    assert isinstance(evaluator, EvaluatePrequential)
    measurements = evaluator.get_mean_measurements()
    for i, model_name in enumerate(model_names):
        print(f"Model {model_name}")
        cm = measurements[i].confusion_matrix
        print("Recall per class")
        for j in range(cm.n_classes):
            recall = cm.data[(j, j)] / cm.sum_row[j] \
                if cm.sum_row[j] != 0 else 'Ill-defined'
            print(f'Class {j}: {recall}')

    print(measurements[0].accuracy_score())
    print(measurements[0].kappa_m_score())
    print(measurements[0].kappa_t_score())
    print('Recall:', measurements[0].recall_score(0))
    print('Recall:', measurements[0].recall_score(1))
    print(evaluator.running_time_measurements[0].get_current_total_running_time())


def experiment1():
    stream = FileStream('datasets/data_n30000.csv')

    nb = NaiveBayes()
    ht = HoeffdingTreeClassifier()
    knn = KNNClassifier()
    mk = my_knn.MyKNNClassifier()
    mk_w = my_knn.MyKNNClassifier(weighted_vote=True)
    mk_s = my_knn.MyKNNClassifier(standardize=True, standardization_debug=False)
    mk_ws = my_knn.MyKNNClassifier(standardize=True, weighted_vote=True)

    metrics = ['accuracy', 'kappa', 'kappa_m', 'kappa_t',
               'running_time', 'model_size']

    models_all = [nb, ht, knn, mk_w, mk_s]
    model_names_all = ['NB', 'HT', 'kNN', 'kNN-W', 'kNN-S']

    model_single = [mk_ws]
    model_name_single = ['MKWS']

    models_three = [knn, mk_w, mk_s]
    model_names_three = ['KNN', 'MKW', 'MKS']

    models = models_all
    model_names = model_names_all

    evaluator = EvaluatePrequential(max_samples=30000,
                                    n_wait=100,
                                    show_plot=False,
                                    metrics=metrics)
    evaluator.evaluate(stream=stream,
                       model=models,
                       model_names=model_names)

    show_recall(evaluator, model_names)

    # print(evaluator.get_mean_measurements(model_names.index('NB')).confusion_matrix)
    generate_latex_tabular(model_names, evaluator, ['accuracy', 'recall', 'kappa_m', 'kappa_t', 'running_time'])


def experiment2():
    stream = FileStream('datasets/data_n30000.csv')

    metrics = ['accuracy', 'kappa_m', 'kappa_t',
               'running_time', 'model_size']

    models = []
    model_names = []
    model_row_names = []

    for w in (100, 1000):
        for k in (5, 10):
            model_names.append(f'l{w}_k{k}')
            models.append(KNNClassifier(max_window_size=w, n_neighbors=k))
            model_row_names.append(f'kNN (w{w},k{k})')

    for w in (10, 50, 100, 1000):
        for k in (1, 3, 5, 10):
            model_names.append(f'l{w}_k{k}_w')
            models.append(my_knn.MyKNNClassifier(max_window_size=w, n_neighbors=k, weighted_vote=True))
            model_row_names.append(f'kNN-W (w{w},k{k})')

    for w in (100, 1000):
        for k in (5, 10):
            model_names.append(f'l{w}_k{k}_s')
            models.append(my_knn.MyKNNClassifier(max_window_size=w, n_neighbors=k, standardize=True))
            model_row_names.append(f'kNN-S (w{w},k{k})')

    for w in (10, 50):
        for k in (1, 3):
            model_names.append(f'l{w}_k{k}')
            models.append(KNNClassifier(max_window_size=w, n_neighbors=k))
            model_row_names.append(f'kNN (w{w},k{k})')

    evaluator = EvaluatePrequential(max_samples=30000,
                                    n_wait=100,
                                    show_plot=False,
                                    metrics=metrics)
    evaluator.evaluate(stream=stream,
                       model=models,
                       model_names=model_names)

    show_recall(evaluator, model_names)
    generate_latex_tabular(model_row_names, evaluator, [
        'accuracy', 'recall', 'kappa_m', 'kappa_t', 'running_time'])


def experiment3():
    stream = FileStream('datasets/covtype_numeric.csv')

    metrics = ['accuracy', 'kappa_m', 'kappa_t',
               'running_time', 'model_size']

    nb = NaiveBayes()
    ht = HoeffdingTreeClassifier()
    knn = KNNClassifier(n_neighbors=5, max_window_size=2000)
    mk = my_knn.MyKNNClassifier()
    mk_w = my_knn.MyKNNClassifier(weighted_vote=True)
    mk_s = my_knn.MyKNNClassifier(standardize=True, standardization_debug=False)

    lb = LeveragingBaggingClassifier(base_estimator=KNNClassifier(n_neighbors=5, max_window_size=2000),
                                     n_estimators=2)
    oba = OzaBaggingADWINClassifier(base_estimator=KNNADWINClassifier(n_neighbors=5, max_window_size=2000),
                                    n_estimators=2)
    kad = KNNADWINClassifier(n_neighbors=5, max_window_size=1000)
    nc = KNNClassifier(n_neighbors=1, max_window_size=1)

    models = [nb, ht, nc, knn, mk_w, mk_s, lb, oba]
    model_names = ['NB', 'HT', 'NC', 'kNN', 'kNN-W', 'kNN-S', 'LB', 'OBA']

    evaluator = EvaluatePrequential(max_samples=100000,
                                    n_wait=2000,
                                    show_plot=True,
                                    metrics=metrics)
    evaluator.evaluate(stream=stream,
                       model=models,
                       model_names=model_names)

    show_recall(evaluator, model_names)
    generate_latex_tabular(model_names, evaluator, [
        'accuracy', 'kappa_m', 'kappa_t', 'running_time'], n_classes=8)
    generate_latex_tabular(model_names, evaluator, ['recall'], n_classes=8)


def generate_latex_tabular(model_row_names, evaluator, metrics, n_classes=2):
    assert isinstance(evaluator, EvaluatePrequential)
    metric_measure_map = {}
    metric_format_map = {}
    metric_header_map = {}
    metric_order_map = {}

    def register_map(key, measure, fmt, header, order):
        metric_measure_map[key] = measure
        metric_format_map[key] = fmt
        metric_header_map[key] = header
        metric_order_map[key] = order

    register_map('accuracy', lambda _e, _i: _e.get_mean_measurements(_i).accuracy_score,
                 fmt=r'{:.2f}', header=r'Accuracy', order='desc')
    register_map('kappa_m', lambda _e, _i: _e.get_mean_measurements(_i).kappa_m_score,
                 fmt=r'{:.2f}', header=r'Kappa M', order='desc')
    register_map('kappa_t', lambda _e, _i: _e.get_mean_measurements(_i).kappa_t_score,
                 fmt=r'{:.2f}', header=r'Kappa T', order='desc')
    register_map('running_time', lambda _e, _i: _e.running_time_measurements[_i].get_current_total_running_time,
                 fmt=r'{:.2f}', header=r'Time (s)', order='asc')
    register_map('recall', lambda _e, _i: _e.get_mean_measurements(_i).recall_score,
                 fmt=r'{:.2f}', header=r'Recall', order='desc')

    mmm = metric_measure_map
    fmt = metric_format_map
    table = []
    for i, row_name in enumerate(model_row_names):
        data = []
        for metric in metrics:
            scale = 1. if metric == 'running_time' else 100.
            if metric == 'recall':
                for cls_id in range(n_classes):
                    datum = fmt[metric].format(scale * mmm[metric](evaluator, i)(cls_id))
                    data.append(datum)
            else:
                datum = fmt[metric].format(scale * mmm[metric](evaluator, i)())
                data.append(datum)
        table.append([float(x) for x in data])

    extended_metrics = '&'.join([m if m != 'recall' else '&'.join([
        f'{m} {i}' for i in range(n_classes)]) for m in metrics
                                 ]).split('&')

    max_at = np.argmax(table, axis=0)
    min_at = np.argmin(table, axis=0)
    is_desc = [metric_order_map[m.split()[0]] == 'desc' for m in extended_metrics]
    highlight_at = [max_at[i] if c else min_at[i] for i, c in enumerate(is_desc)]
    highlighted_table = [[model_row_names[i]] + [
        r'\textbf{' + str(cell) + r'}' if highlight_at[j] == i else str(cell)
        for j, cell in enumerate(row)
    ] for i, row in enumerate(table)]

    header_line = 'Algorithm & ' + ' & '.join(
        [metric_header_map[m] if m != 'recall' else ' & '.join([
            f'{metric_header_map[m]} {i}' for i in range(n_classes)])
         for m in metrics]
    )

    tex = [header_line + ' \\\\\n\\hline']
    tex.extend([' & '.join(row) + r' \\' for row in highlighted_table])

    print('\n'.join(tex))

    return header_line, table


def visualize1():
    stream = FileStream('datasets/data_n30000.csv')

    cm = matplotlib.colors.ListedColormap(['purple', 'yellow'])
    x, y = stream.next_sample(30000)

    mean_x0 = x[:, 0].mean(axis=0)
    mean_x1 = x[:, 1].mean(axis=0)
    std_x0 = x[:, 0].std(axis=0)
    std_x1 = x[:, 1].std(axis=0)

    plt.scatter(x[:, 0], x[:, 1], s=1, c=y, alpha=0.9, cmap=cm)
    plt.xlabel('X[0]')
    plt.ylabel('X[1]')
    plt.text(-650, 1670, f'X[0]: mean={mean_x0:.2f}, std={std_x0:.2f}')
    plt.text(-650, 1550, f'X[1]: mean={mean_x1:.2f}, std={std_x1:.2f}')
    # plt.text(-650, 1650, f'mean=({mean_x0:.2f}, {mean_x1:.2f}), '
    #                      f'std=({std_x0:.2f}, {std_x1:.2f})')
    plt.colorbar()
    plt.show()

    x_zoomed = np.array([_x for _x in x if (800 < _x[0] < 900) and (0 < _x[1] < 100)])
    y_zoomed = [_y for _x, _y in zip(x, y) if (800 < _x[0] < 900) and (0 < _x[1] < 100)]
    plt.scatter(x_zoomed[:, 0], x_zoomed[:, 1], s=5, c=y_zoomed, alpha=0.9, cmap=cm)
    plt.xlabel('X[0]')
    plt.ylabel('X[1]')
    plt.colorbar()
    plt.show()

    plt.subplots_adjust(hspace=0.5)
    plt.subplot(221)
    plt.hist(x[y == 0, 0], 500, color='purple')
    plt.title('X[0] histogram of class 0 instances')

    plt.subplot(222)
    plt.hist(x[y == 0, 1], 500, color='purple')
    plt.title('X[1] histogram of class 0 instances')

    plt.subplot(223)
    plt.hist(x[y == 1, 0], 500, color='yellow')
    plt.title('X[0] histogram of class 1 instances')

    plt.subplot(224)
    plt.hist(x[y == 1, 1], 500, color='yellow')
    plt.title('X[1] histogram of class 1 instances')

    plt.show()
#
#
# def analyze_concept_drift():
#     from skmultiflow.drift_detection.adwin import ADWIN
#     adwin = ADWIN()
#
#     data_stream = FileStream('datasets/covtype_numeric.csv')
#
#     # Adding stream elements to ADWIN and verifying if drift occurred
#     for i in range(2000):
#         sample = data_stream.next_sample()
#         adwin.add_element()
#         if adwin.detected_change():
#             print('Change detected in data: ' + str(data_stream[i]) + ' - at index: ' + str(i))


if __name__ == '__main__':
    plt.rcParams['figure.dpi'] = 192
    # experiment1()
    # experiment2()
    # experiment3()
    visualize1()
